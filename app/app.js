var invoicesApp = angular.module('invoicesApp', ['ui.router', 'ngResource', 'smart-table', 'ngCookies', 'ui.bootstrap']);

invoicesApp.config(function($provide, $stateProvider,  $urlRouterProvider, $httpProvider) {
    $httpProvider.interceptors.push('AuthInterceptor')
    $urlRouterProvider.otherwise('/');
    $stateProvider
        .state('main', {
            url: '/',
            templateUrl: "views/main.html",
            secure: true
        })
        .state('login', {
            url: '/login?redirectReason',
            templateUrl: "views/login.html",
            controller: 'LoginCtrl',
            secure: false
        })
        .state('register', {
            url: '/register',
            templateUrl: "views/register.html",
            controller: 'RegisterCtrl',
            secure: false
        })
        .state('activateUser', {
            url: '/activate',
            templateUrl: "views/activate.html",
            controller: 'ActivateCtrl',
            secure: false
        })
        .state('logout', {
            url: '/logout',
            controller: 'LogoutCtrl',
            secure: true
        })
        .state('companyConfig', {
            url: '/config/company?redirectReason',
            controller: 'CompanyConfigCtrl',
            templateUrl: "views/companyConfig.html",
            secure: true
        })
        .state('userConfig', {
            url: '/config/user',
            controller: 'UserConfigCtrl',
            templateUrl: "views/userConfig.html",
            secure: true
        })
        .state('reportsConfig', {
            url: '/config/reports',
            controller: 'ReportsConfigCtrl',
            templateUrl: "views/reportsConfig.html",
            secure: true
        })
        .state('invoices', {
            url: '/invoices?client_nip&client_name',
            templateUrl: "views/invoices.html",
            controller: 'InvoicesCtrl',
            secure: true
        })
        .state('invoiceDetail', {
            url: '/invoices/:invoiceId',
            templateUrl: "views/invoiceDetail.html",
            controller: 'InvoiceDetailCtrl',
            secure: true
        })
        .state('clients', {
            url: '/clients',
            templateUrl: "views/clients.html",
            controller: 'ClientsCtrl',
            secure: true
        });
    $provide.decorator('$state', function($delegate, $stateParams) {
        $delegate.forceRefresh = function() {
            return $delegate.go($delegate.current, $stateParams, {
                reload: true,
                inherit: false,
                notify: true
            });
        };
        return $delegate;
    });
    });
invoicesApp.constant('invoicesServiceUri', 'http://localhost:8200/')

invoicesApp.run(function ($rootScope, $state, CredentialsService) {
    $rootScope.$on("$stateChangeStart", function(e, toState){
        if (toState.secure && !CredentialsService.haveToken()){
            // User isn’t authenticated
            $state.transitionTo("login", {redirectReason: "unauthorized"})
            e.preventDefault();
        } else {
        }
        $rootScope.secureState = toState.secure;
    });
});