invoicesApp.service('CredentialsService', ['$cookies', function($cookies) {
    this.setToken = function(token) {
        $cookies.put('token', token);
    };
    this.haveToken = function() {
        return $cookies.get('token');
    };
    this.getToken = function() {
        return $cookies.get('token');
    }
    this.removeToken = function() {
        $cookies.remove('token');
    }
}]);