invoicesApp.service('ClientsService', ['$http', 'invoicesServiceUri', '$resource', function($http, invoicesServiceUri, $resource) {
    var ClientResource = $resource(invoicesServiceUri + 'client');
    this.getClients = function() {
        //console.log(ClientResource.query().$promise);
        return ClientResource.query().$promise;
    }
}]);