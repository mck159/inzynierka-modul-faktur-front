console.log('loading interceptor');
invoicesApp.service('AuthInterceptor', ['CredentialsService', '$q', '$injector', function(CredentialsService, $q, $injector) {
    this.request = function(config) {
        console.log("OK");
        console.log(config);
        console.log('intercepting');
        if(config.url.indexOf("auth") == -1) { //TODO
            //config.headers["X-UserId"] = "190";
        }
        config.headers['X-Token'] = CredentialsService.getToken();
        return config;
    };
    this.responseError = function(config) {
        var state = $injector.get('$state');
        if(config.status == 401 && state.current.secure) {
            var inCompanyConfigState = state.current != state.get('companyConfig');
            if(!inCompanyConfigState || (inCompanyConfigState && config.data.status != "COMPANY_INACTIVE")) {
                state.transitionTo('login', {redirectReason: "unauthorized"});
            }
            if(config.data.status == "COMPANY_INACTIVE") {
                state.transitionTo('companyConfig', {redirectReason: "inactive"});
            }
        }
        return $q.reject(config);
    };
}]);