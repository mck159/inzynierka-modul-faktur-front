invoicesApp.service('UserConfigService', ['$http', 'invoicesServiceUri', '$resource', function($http, invoicesServiceUri, $resource) {
    var UserResource = $resource(invoicesServiceUri + 'user',{}, {update: {method : 'PUT'}});

    this.updateUser = function(user) {
        return user.$update();
    }
    this.find = function() {
        return UserResource.get({}).$promise;
    }
}]);