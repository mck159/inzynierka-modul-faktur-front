invoicesApp.service('InvoicesService', ['$http', 'invoicesServiceUri', '$resource', function($http, invoicesServiceUri, $resource) {

    var InvoiceResource = $resource(invoicesServiceUri + 'invoice/:id',{id: "@_id"}, {update: {method : 'PUT'}});

    this.saveInvoice = function(invoice) {
        return invoice.$save();
    }
    this.deleteInvoice = function(invoice) {
        return invoice.$delete({id: invoice.id});
    }
    this.submitEdit = function(invoice) {
        return invoice.$update({id: invoice.id}, invoice);
    }
    this.find = function(invoice_id) {
        return InvoiceResource.get({id : invoice_id}).$promise;
    }
    this.createNew = function() {
        return new InvoiceResource();
    }
}]);