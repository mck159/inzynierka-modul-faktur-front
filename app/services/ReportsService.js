invoicesApp.service('ReportsService', ['$http', 'invoicesServiceUri', '$resource', function($http, invoicesServiceUri, $resource) {
    var ReportResource = $resource(invoicesServiceUri + 'invoice/report',{type: "@_type"});
    console.log(ReportResource);
    this.add = function(report) {
        return report.$save();
    }
    this.findAll = function() {
        return ReportResource.query().$promise;
    }
    this.remove = function(report) {
        return report.$delete({type: report.type});
    }
    this.createNew = function() {
        return new ReportResource();
    }
}]);