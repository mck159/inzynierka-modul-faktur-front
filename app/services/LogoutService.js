invoicesApp.service('LogoutService', ['$http', 'invoicesServiceUri', '$resource', function($http, invoicesServiceUri, $resource) {
    var authUrl = invoicesServiceUri + "auth/logout";
    this.logout = function(username, password) {
        return $http.post(authUrl);
    }
}]);