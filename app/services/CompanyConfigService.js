invoicesApp.service('CompanyConfigService', ['$http', 'invoicesServiceUri', '$resource', function($http, invoicesServiceUri, $resource) {
    var CompanyResource = $resource(invoicesServiceUri + 'company',{}, {update: {method : 'PUT'}});

    this.updateCompany = function(company) {
        return company.$update();
    }
    this.find = function() {
        return CompanyResource.get({}).$promise;
    }
}]);