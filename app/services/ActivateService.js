invoicesApp.service('ActivateService', ['$http', 'invoicesServiceUri', '$resource', function($http, invoicesServiceUri, $resource) {

    this.activate = function(username, password, token) {
        var activateUrl = invoicesServiceUri + "auth/confirm";
        return $http.post(activateUrl, {name: username, password: password, confirmationToken: token});
    }
    this.resendToken = function(username, password) {
        var resendUrl = invoicesServiceUri + "auth/resendConfirmation";
        return $http.post(resendUrl, {name: username, password: password});
    }
}]);