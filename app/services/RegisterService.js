invoicesApp.service('RegisterService', ['$http', 'invoicesServiceUri', '$resource', function($http, invoicesServiceUri, $resource) {
    var registerUrl = invoicesServiceUri + "user";
    this.register = function(username, email, password) {
        return $http.post(registerUrl, {name: username, email: email, password: password});
    }
}]);