invoicesApp.service('LoginService', ['$http', 'invoicesServiceUri', '$resource', function($http, invoicesServiceUri, $resource) {
    var authUrl = invoicesServiceUri + "auth";
    console.log(authUrl);
    this.authorize = function(username, password) {
        //console.log(ClientResource.query().$promise);
        return $http.post(authUrl, {name: username, password: password});
    }
}]);