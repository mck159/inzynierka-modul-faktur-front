invoicesApp.controller('ActivateCtrl', ['$scope', '$state', 'ActivateService', function ActivateCtrl($scope, $state, ActivateService) {
    $scope.activate = function() {
        ActivateService.activate($scope.username, $scope.password, $scope.token).then(
            function(data) {
                $state.go('login', {redirectReason: 'activated'});
            },
            function(error) {
                $scope.wrongCredentials = false;
                $scope.wrongToken = false;
                $scope.wrongPassword = false;
                $scope.noUser = false;
                switch(error.status) {
                    case 400:
                        $scope.wrongCredentials = true;
                        break;
                    case 401:
                        switch(error.data.status) {
                            case 'WRONG_TOKEN':
                                $scope.wrongToken = true;
                                break;
                            case "WRONG_PASSWORD":
                                $scope.wrongPassword = true;
                                break;
                        }
                        break;
                    case 404:
                        $scope.noUser = true;
                        break;
                }
            }
        )
    }
    $scope.resendToken = function() {
        $scope.tokenResended = true;
        ActivateService.resendToken($scope.username, $scope.password    ).then(
            function(data) {
                console.log(data);
                //$state.go('login', {redirectReason: 'activated'});
            },
            function(error) {
                $scope.wrongCredentials = false;
                $scope.wrongPassword = false;
                $scope.unauthorized = false;
                $scope.noUser = false;
                switch(error.status) {
                    case 400:
                        $scope.wrongCredentials = true;
                        break;
                    case 401:
                        switch(error.data.status) {
                            case 'WRONG_TOKEN':
                                $scope.wrongToken = true;
                                break;
                            case "WRONG_PASSWORD":
                                $scope.wrongPassword = true;
                                break;
                        }
                        break;
                    case 404:
                        $scope.noUser = true;
                        break;
                }
            })
    }
}]);
