invoicesApp.controller('RegisterCtrl', ['$scope', '$resource', '$state', '$stateParams', 'RegisterService', 'CredentialsService', 'invoicesServiceUri', function RegisterCtrl($scope, $resource, $state, $stateParams, RegisterService, CredentialsService, invoicesServiceUri) {
    $scope.register = function() {
        $scope.passwordsNoMatch = false;
        $scope.validationData = {};
        if($scope.password != $scope.passwordConfirm) {
            $scope.passwordsNoMatch = true;
            return;
        }
        RegisterService.register($scope.name, $scope.email, $scope.password).then(
            function(data) {
                $state.go('activateUser');
            },
            function(error) {
                var validationData = {}
                for(i = 0 ; i < error.data.length ; i++) {
                    validationData[error.data[i].field] = error.data[i].message;
                }
                $scope.validationData = validationData;
            }
        )
    }
}]);
