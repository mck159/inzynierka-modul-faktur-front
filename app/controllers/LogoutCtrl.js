invoicesApp.controller('LogoutCtrl', ['$scope', '$state', 'LogoutService', 'CredentialsService', function LogoutCtrl($scope, $state, LogoutService, CredentialsService) {
    LogoutService.logout().then(
        function(data) {
            CredentialsService.removeToken();
            $state.go('login', {redirectReason: 'logout'});
        },
        function(error) {
            alert('logout error!');
        }
    );
}]);
