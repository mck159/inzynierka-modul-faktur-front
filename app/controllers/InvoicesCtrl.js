invoicesApp.controller('InvoicesCtrl', ['$scope', '$resource', '$state', 'InvoicesService', 'invoicesServiceUri', function InvoicesCtrl($scope, $resource, $state, InvoicesService, invoicesServiceUri) {
    var InvoiceResource = $resource(invoicesServiceUri + 'invoice/:id',{id: "@_id"}, {update: {method : 'PUT'}});
    var invoices = InvoiceResource.query({client_nip: $state.params.client_nip});
    for(i = 0 ; i < invoices.length ; i++) {
        invoices[i].editable=false;
    }
    console.log($state.params)

    if($state.params.client_nip || $state.params.client_name) {
        $scope.invoicesClient = {name: $state.params.client_name, nip: $state.params.client_nip};
    }

    $scope.invoices = invoices;
    $scope.addInvoice = function() {
        var inv = new InvoiceResource();
        inv.editable = true;
        inv.new = true;
        inv.items=[];
        $scope.invoices.push(inv)

    }
}]);
