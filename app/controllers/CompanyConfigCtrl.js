invoicesApp.controller('CompanyConfigCtrl', ['$scope', '$resource', '$state', '$stateParams', 'CompanyConfigService', 'CredentialsService', 'invoicesServiceUri', function CompanyConfigCtrl($scope, $resource, $state, $stateParams, CompanyConfigService, CredentialsService, invoicesServiceUri) {
    $scope.redirectReason = $stateParams.redirectReason;
    CompanyConfigService.find().then(
        function(company) {
            $scope.company = company;
        },
        function() {
        }
    )

    $scope.update = function() {
        $scope.successUpdate = false;
        $scope.validationData = {}
        CompanyConfigService.updateCompany($scope.company).then(
            function(data) {
                $scope.redirectReason = null;
                $scope.successUpdate = true;
                CompanyConfigService.find().then(
                    function(company) {
                        $scope.company = company;
                    },
                    function() {
                    }
                )
            },
            function(error) {
                var validationData = {}
                for(i = 0 ; i < error.data.length ; i++) {
                    validationData[error.data[i].field] = error.data[i].message;
                }
                console.log($scope.validationData);
                $scope.validationData = validationData;
            }
        );
    }
}]);
