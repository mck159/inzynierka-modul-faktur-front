invoicesApp.controller('LoginCtrl', ['$scope', '$resource', '$state', '$stateParams', 'LoginService', 'CredentialsService', 'invoicesServiceUri', function LoginCtrl($scope, $resource, $state, $stateParams, LoginService, CredentialsService, invoicesServiceUri) {
    $scope.redirectReason = $stateParams.redirectReason;
    $scope.login = function() {
        console.log("LOGIN");
        console.log($stateParams);
        LoginService.authorize($scope.username, $scope.password)
            .then(
                function (data) {
                    if(data.status == 200) {
                        CredentialsService.setToken(data.data.token);
                        $state.go('invoices');
                    } else {
                        alert("CRITICAL ERROR");
                    }
                },
                function (error) {
                    $scope.wrongCredentials = false;
                    $scope.unauthorized = false;
                    $scope.noUser = false;
                    switch(error.status) {
                        case 400:
                            $scope.wrongCredentials = true;
                            break;
                        case 401:
                            if(error.data.status == "EMAIL_NOT_CONFIRMED") {
                                $state.go('activateUser');
                            }
                            $scope.unauthorized = true;
                            break;
                        case 404:
                            $scope.noUser = true;
                            break;
                    }
                }
            );
    }
}]);
