invoicesApp.controller('ClientsCtrl', ['$scope', '$resource', '$state', 'ClientsService', 'invoicesServiceUri', function InvoicesCtrl($scope, $resource, $state, ClientsService, invoicesServiceUri) {
    ClientsService.getClients()
        .then(
            function(clients) {
                $scope.clients = clients;
            },
            function(error) {
                console.error(error);
            }
        );
}]);
