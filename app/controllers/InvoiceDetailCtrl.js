invoicesApp.controller('InvoiceDetailCtrl', ['$scope', '$stateParams', '$resource', '$state', 'InvoicesService', 'invoicesServiceUri', function InvoicesCtrl($scope, $stateParams, $resource, $state, InvoicesService, invoicesServiceUri) {
    $scope.validateMoney = function(v,prop) {
        if(v[prop] === '') return;
        var regexMoney = /^\d*(\.\d{0,2})?$/;
        var numStr = "123.20";
        if (!regexMoney.test(v[prop])) {
            v[prop] = 0.00;
            alert("Invalid money format!");
        }
    }
    $scope.validateVat = function(v,prop) {
        if(v[prop] === '') return;
        var num = ~~Number(v[prop]);
        if(String(num) !== v[prop] || num < 0 || num > 100) {
            v[prop] = 0.00;
            alert('Invalid vat format (should be positive number between 0 and 100)')
        }
    }
    $scope.loadInvoice = function() {
        InvoicesService.find($stateParams.invoiceId)
            .then(
                function (invoice) {
                    $scope.invoice = invoice;
                },
                function (error) {
                    console.error(error);
                }
            )
    }

    $scope.refreshInvoiceAmounts = function() {
        var amountGross = 0.0;
        var amountPaid = 0.0;
        if($scope.invoice.payments) {
            for(var i = 0 ; i < $scope.invoice.payments.length ; i++) {
                var payment = $scope.invoice.payments[i];
                if (payment.amount) {
                    amountPaid += parseFloat(payment.amount);
                }
            }
        }
        if($scope.invoice.items) {
            for(var i = 0 ; i < $scope.invoice.items.length ; i++) {
                var item = $scope.invoice.items[i];
                if (item.priceNet && item.vat) {
                    amountGross += parseFloat(item.priceNet * (1 + item.vat/100));
                }
            }
        }
        $scope.invoice.amount = amountGross;
        $scope.invoice.paidAmount = amountPaid;
    }


    if($stateParams.invoiceId == 'new') {
        $scope.newInvoice = true;
        $scope.editMode = true;
        $scope.invoice = InvoicesService.createNew();
        $scope.invoice.client = {}
        $scope.invoice.seller = {}
        $scope.invoice.items = [{}]
        $scope.invoice.amount = 0;
        $scope.invoice.paidAmount = 0;
    } else {
        $scope.newInvoice = false;
        $scope.editMode = false;
        $scope.loadInvoice();
    }


    $scope.deleteInvoice= function() {
        if(window.confirm('Are you sure?')) {
            InvoicesService.deleteInvoice($scope.invoice);
            $state.go('invoices');
        }
    }
    $scope.saveInvoice = function() {
        if(window.confirm('Are you sure?')) {
            if($scope.newInvoice) {
                $scope.addInvoice();
            } else {
                InvoicesService.submitEdit($scope.invoice).then(
                    function(data) {
                        alert('ok');
                        $scope.loadInvoice();

                    }, function(error) {
                        console.log("ERROR");
                        console.log(error);
                        $scope.validationData = $scope.prepareValidationStructure(error);
                        $scope.editMode = true;
                    }
                );
            }
        }
    }
    $scope.addInvoice = function() {
        InvoicesService.saveInvoice($scope.invoice).then(
            function(data) {
                $scope.go('invoiceDetail', {invoiceId: data.data});
            },
            function(error) {
                $scope.validationData = $scope.prepareValidationStructure(error);
                $scope.editMode = true;
            }
        );
    }
    $scope.addNewPayment = function() {
        if(!$scope.invoice.payments) {
            $scope.invoice.payments = [];
        }
        $scope.invoice.payments.push({});
    }
    $scope.addNewItem = function() {
        if(!$scope.invoice.items) {
            $scope.invoice.items = [];
        }
        $scope.invoice.items.push({});
    }
    $scope.startEdit = function() {
        $scope.editMode = true;
    }
    $scope.cancelEdit = function() {
        $scope.editMode = false;
        $state.reload();
    }
    $scope.removePayment = function(payment) {
        $scope.invoice.payments.splice($scope.invoice.payments.indexOf(payment), 1);
        console.log($scope.invoice);
    }

    $scope.prepareValidationStructure= function(error) {
        console.info(error);
        if(!error) {
            return;
        }
        var validationData = {}
        for(i = 0 ; i < error.data.length ; i++) {
            validationData[error.data[i].field] = error.data[i].message;
        }
        return validationData;
    }
}]);
