invoicesApp.controller('ReportsConfigCtrl', ['$scope', '$resource', '$state', 'ReportsService', function ReportsConfigCtrl($scope, $resource, $state, ReportsService) {
    $scope.reportSummary = ReportsService.createNew();
    $scope.reportRemindOverdue = ReportsService.createNew();
    $scope.reportSummary.type='SUMMARY';
    $scope.reportRemindOverdue.type='REMIND_OVERDUE';
    $scope.gotSummary = false;
    $scope.gotRemindOverdue = false;
    var x = ReportsService.findAll().then(
        function(data) {
            for(i = 0 ; i < data.length ; i++) {
                console.log(data[i].type);
                if(data[i].type == "SUMMARY") {
                    $scope.reportSummary = data[i];
                    $scope.reportSummary.type='SUMMARY';
                    $scope.gotSummary = true;
                    console.log($scope.reportSummary)
                }
                else if(data[i].type == "REMIND_OVERDUE") {
                    $scope.reportRemindOverdue = data[i];
                    $scope.reportRemindOverdue.type='REMIND_OVERDUE';
                    console.log($scope.reportRemindOverdue)
                    $scope.gotRemindOverdue = true;
                }
            }
        },
        function(error) {
            //alert('error');
        }
    );
    console.log(x);
    $scope.applyReportRemindOverdue = function() {
        ReportsService.add($scope.reportRemindOverdue);
        $state.go($state.current, {}, {reload: true});
    }
    $scope.applyReportSummary = function() {
        ReportsService.add($scope.reportSummary);
        $state.go($state.current, {}, {reload: true});
    }
    $scope.deleteReportRemindOverdue = function() {
        ReportsService.remove($scope.reportRemindOverdue);
        $state.go($state.current, {}, {reload: true});
    }
    $scope.deleteReportSummary = function() {
        ReportsService.remove($scope.reportSummary);
        $state.go($state.current, {}, {reload: true});
    }
}]);
