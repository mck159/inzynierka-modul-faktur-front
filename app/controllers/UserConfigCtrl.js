invoicesApp.controller('UserConfigCtrl', ['$scope', '$resource', '$state', '$stateParams', 'UserConfigService', 'CredentialsService', 'invoicesServiceUri', function UserConfigCtrl($scope, $resource, $state, $stateParams, UserConfigService, CredentialsService, invoicesServiceUri) {
    UserConfigService.find().then(
        function(user) {
            $scope.user = user;
        },
        function(error) {
            alert('Getting user error');
        }
    )
    $scope.updateUser = function() {
        $scope.successUpdate = false;
        $scope.passwordsNoMatch = false;
        $scope.validationData = {};
        if(!$scope.user.password) {
            $scope.user.password = null;
        }
        if($scope.user.password && $scope.user.password != $scope.passwordConfirm) {
            $scope.passwordsNoMatch = true;
            return;
        }
        UserConfigService.updateUser($scope.user).then(
            function(data) {
                $scope.successUpdate = true;
                UserConfigService.find().then(
                    function(user) {
                        $scope.user = user;
                    },
                    function(error) {
                        alert('Getting user error');
                    }
                )
            },
            function(error) {
                var validationData = {}
                for(i = 0 ; i < error.data.length ; i++) {
                    validationData[error.data[i].field] = error.data[i].message;
                }
                console.log($scope.validationData);
                $scope.validationData = validationData;

            }
        )
    }
}]);
